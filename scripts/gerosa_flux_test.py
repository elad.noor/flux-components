# MIT License
#
# Copyright (c) 2019 Elad Noor
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from uncertainties import nominal_value, std_dev

from equilibrator_api import ComponentContribution, Q_
from flux_components.settings import RESULT_DIR
from flux_components.flux_component_analysis import FluxComponentAnalysis

fca = FluxComponentAnalysis()

equilibrator = ComponentContribution(
    p_h=Q_(7.0), ionic_strength=Q_("0.1M")
)

# %%
fig = fca.TestBiomassFunction(sources=['Gerosa_2015'])
fig.savefig(os.path.join(RESULT_DIR, 'gerosa_diviation_biomass.pdf'))

##%%
# met = 'NADPH'
# bmc = BiomassComposition('e_coli')
# s = fca.flux_imbalance[fca.flux_imbalance['serial number'] >= 117]
# s = s.loc[s.metabolite == met, ['serial number', 'flux']]
# s = s.join(fca.meta_df['growth rate [h-1]'], on='serial number')
# s['required per gr'] = s['growth rate [h-1]'].apply(lambda gr: bmc.GetComposition(gr).loc[met])
# s['required'] = s['required per gr'] * s['growth rate [h-1]']
#
# fig, ax = plt.subplots(1, 1, figsize=(6, 6))
# ax.plot(s['required'].apply(nominal_value), s['flux'].apply(nominal_value), '.')
# ax.plot([1, 50], [1, 50], '--')
# ax.set_xscale('log')
# ax.set_yscale('log')
# ax.set_aspect('equal')
#

# %%
merged_df = fca.meta_df.copy()
df = merged_df[merged_df['person'] == 'Luca Gerosa']

# fig, ax = plt.subplots(1, 1, figsize=(8, 8))
##x = 'glc_uptake'; xlabel = 'glucose uptake rate [mmol gCDW$^{-1}$ h$^{-1}$]'
# x = 'growth rate [h-1]'; xlabel = 'growth rate [h-1]'
# y = '$g^{diss}$ [kJ gCDW$^{-1}$ h$^{-1}$]'
#
# ax.set_prop_cycle(color=sns.color_palette('Paired', n_colors=11))
# for gr, gr_df in df.groupby('medium'):
#    ax.errorbar(gr_df[x].apply(nominal_value),
#                np.abs(gr_df[y].apply(nominal_value)),
#                xerr=(gr_df[x].apply(std_dev)),
#                yerr=(gr_df[y].apply(std_dev)),
#                fmt='o', linewidth=0.5,
#                markersize=10, label=gr)
#
# ax.set_xlim(0, None)
# ax.set_ylim(0, None)
# ax.set_xlabel(xlabel)
# ax.set_ylabel(y)
# ax.legend(loc='upper left')
# fig.savefig(os.path.join(S.RESULT_DIR, 'gerosa_gdiss.pdf'))

#%%
df["growth rate [h-1]"] = df.growth_rate.apply(
    lambda x: x.to("1/hour").magnitude)

heat_units_label = str(FluxComponentAnalysis.E_DISS_UNITS)

df[f'$g^{{diss}}$ [{heat_units_label}]'] = \
    df.energy_dissipation_rate.apply(
        lambda x: x.to(FluxComponentAnalysis.E_DISS_UNITS).magnitude
    )

df[f'biomass $h^{{comb}}$ [{heat_units_label}]'] = \
    df.biomass_heat_of_combustion_rate.apply(
        lambda x: x.to(FluxComponentAnalysis.E_DISS_UNITS).magnitude
    )

df[f'fermentation $h^{{comb}}$ [{heat_units_label}]'] = \
    df.fermentation_heat_of_combustion.apply(
        lambda x: x.to(FluxComponentAnalysis.E_DISS_UNITS).magnitude
    )

df[f'precursor $h^{{comb}}$ [{heat_units_label}]'] = \
    df.precursor_heat_of_combustion.apply(
        lambda x: x.to(FluxComponentAnalysis.E_DISS_UNITS).magnitude
    )

df[f'import $h^{{comb}}$ [{heat_units_label}]'] = \
    df.import_heat_of_combustion.apply(
        lambda x: x.to(FluxComponentAnalysis.E_DISS_UNITS).magnitude
    )

#%%
fig, axs = plt.subplots(2, 2, figsize=(15, 15))
ax = axs[0, 0]
# x = 'glc_uptake'; xlabel = 'glucose uptake rate [mmol gCDW$^{-1}$ h$^{-1}$]'
x = 'growth rate [h-1]'
y = f'$g^{{diss}}$ [{heat_units_label}]'

ax.set_prop_cycle(color=sns.color_palette('Paired', n_colors=11))
for gr, gr_df in df.groupby('medium'):
    ax.errorbar(gr_df[x].apply(nominal_value),
                np.abs(gr_df[y].apply(nominal_value)),
                xerr=(gr_df[x].apply(std_dev)),
                yerr=(gr_df[y].apply(std_dev)),
                fmt='o', linewidth=0.5,
                markersize=10, label=gr)

ax.set_xlim(0, None)
ax.set_ylim(0, None)
ax.set_xlabel(x)
ax.set_ylabel(y)
ax.legend()

ax = axs[0, 1]
ax.set_prop_cycle(color=sns.color_palette('deep', n_colors=4))

x = 'growth rate [h-1]'
for label in ['biomass $h^{comb}$', 'fermentation $h^{comb}$',
              'precursor $h^{comb}$', '$g^{diss}$']:
    y = f"{label} [{heat_units_label}]"
    ax.errorbar(df[x].apply(nominal_value),
                np.abs(df[y].apply(nominal_value)),
                xerr=(df[x].apply(std_dev)),
                yerr=(df[y].apply(std_dev)),
                fmt='o', linewidth=0.5, label=label, zorder=2,
                markersize=10)

ax.set_xlim(0, None)
ax.set_ylim(0, None)
ax.set_xlabel(x)
ax.set_ylabel(heat_units_label)
_, xmax = ax.axes.get_xlim()
ax.legend(loc='best')

# plot the dissipation as stacked bar plots

ax = axs[1, 0]
ind = df['growth rate [h-1]'].apply(nominal_value).values

bm_col = df[f"biomass $h^{{comb}}$ [{heat_units_label}]"]
fer_col = df[f"fermentation $h^{{comb}}$ [{heat_units_label}]"]
pre_col = df[f"precursor $h^{{comb}}$ [{heat_units_label}]"]
pre_minus_bm = pre_col - bm_col
pre_minus_bm.name = '$g^{diss}$ (precursors -> biomass)'
gdiss_col = df[f"$g^{{diss}}$ [{heat_units_label}]"]
import_col = df[f"import $h^{{comb}}$ [{heat_units_label}]"]
df1 = pd.DataFrame([bm_col, fer_col, pre_minus_bm, gdiss_col, -import_col])
df1 = df1.transpose()

width = max(ind) / 50.0
ax.set_prop_cycle(color=sns.color_palette('deep', n_colors=4))
ax.plot(ind, df1.iloc[:, -1].apply(nominal_value),
        label='carbon source heat of combustion', marker='o', linewidth=0,
        markersize=10, color=(0.8, 0.6, 0.8), zorder=0)

ax.bar(ind, df1.iloc[:, 0].apply(nominal_value),
       width=width, yerr=df1.iloc[:, 0].apply(std_dev),
       bottom=0, label='stored in biomass')
ax.bar(ind, df1.iloc[:, 1].apply(nominal_value),
       width, yerr=df1.iloc[:, 1].apply(std_dev),
       bottom=df1.iloc[:, 0].apply(nominal_value).tolist(),
       label='stored in fermentation')
ax.bar(ind, df1.iloc[:, 2].apply(nominal_value),
       width, yerr=df1.iloc[:, 2].apply(std_dev),
       bottom=df1.iloc[:, 0:2].applymap(nominal_value).sum(axis=1).tolist(),
       label='dissipated (precursors -> biomass)')
ax.bar(ind, df1.iloc[:, 3].apply(nominal_value),
       width, yerr=df1.iloc[:, 3].apply(std_dev),
       bottom=df1.iloc[:, 0:3].applymap(nominal_value).sum(axis=1).tolist(),
       label='dissipated (glucose -> precursors)')

ax.set_xlabel('growth rate [h$^{-1}$]')
ax.legend(loc='best')
ax.set_ylabel('kJ gCDW$^{-1}$ h$^{-1}$')
ax.set_xlim(0, 1)

# ax.plot([0, 1], [0, HEAT_OF_COMBUSTION_BIOMASS], '-')

# convert all dissipation and oxidation energies to units of
# percent of the total (glucose oxidation energy)

df2 = df1.div(df1.iloc[:, -1], axis=0)

ax = axs[1, 1]
ind = df['growth rate [h-1]'].apply(nominal_value).values
width = max(ind) / 50.0
ax.set_prop_cycle(color=sns.color_palette('deep', n_colors=4))
ax.bar(ind, df2.iloc[:, 0].apply(nominal_value),
       width, yerr=df2.iloc[:, 0].apply(std_dev),
       bottom=0,
       label='stored in biomass')
ax.bar(ind, df2.iloc[:, 1].apply(nominal_value),
       width, yerr=df2.iloc[:, 1].apply(std_dev),
       bottom=df2.iloc[:, 0].apply(nominal_value).tolist(),
       label='stored in fermentation')
ax.bar(ind, df2.iloc[:, 2].apply(nominal_value),
       width, yerr=df2.iloc[:, 2].apply(std_dev),
       bottom=df2.iloc[:, 0:2].applymap(nominal_value).sum(axis=1).tolist(),
       label='dissipated (precursors -> biomass)')
ax.bar(ind, df2.iloc[:, 3].apply(nominal_value),
       width, yerr=df2.iloc[:, 3].apply(std_dev),
       bottom=df2.iloc[:, 0:3].applymap(nominal_value).sum(axis=1).tolist(),
       label='dissipated (glucose -> precursors)')

ax.set_xlim(0, 1)
ax.plot([0, 1], [1, 1], '-', linewidth=4, color=(0.8, 0.6, 0.8), zorder=0)
ax.set_xlabel('growth rate [h$^{-1}$]')
ax.set_ylabel('fraction of total input heat of combustion')
ax.legend(loc='best')

fig.show()
fig.tight_layout()
fig.savefig(os.path.join(RESULT_DIR, 'gerosa_gdiss.pdf'))
