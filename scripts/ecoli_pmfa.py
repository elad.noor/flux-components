"""Use PMFA to analyze the E. coli flux data."""
# MIT License
#
# Copyright (c) 2019 Elad Noor
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import itertools
import logging
import numpy as np
import pandas as pd
from random import seed
from scipy.linalg import norm
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import seaborn as sns

from pmfa import PMF
from flux_components.flux_sampler import FluxSampler
from flux_components.settings import RESULT_DIR

#%%

logging.getLogger().setLevel(logging.INFO)

INPUT_FNAME = "src/flux_components/data/meta_analysis.xls"
NUM_REACTIONS = 48

reactions_sheet = pd.read_excel(INPUT_FNAME, sheet_name="reactions", header=0,
                                index_col=0)
S = reactions_sheet.iloc[0:NUM_REACTIONS, 6:].applymap(float).T
S.fillna(0.0, inplace=True)

L = reactions_sheet.iloc[0:NUM_REACTIONS, 4] / 100.0
U = reactions_sheet.iloc[0:NUM_REACTIONS, 5] / 100.0

fluxes_sheet = pd.read_excel(INPUT_FNAME, sheet_name="fluxes", skiprows=1,
                             header=0, index_col=None)
NUM_META_COLUMNS = fluxes_sheet.shape[1] - 2 * NUM_REACTIONS
meta_df = fluxes_sheet.iloc[:, 0:NUM_META_COLUMNS].copy()
meta_df.index = meta_df.index.map(int)
meta_df.index.name = 'serial_number'

growth_rate = fluxes_sheet["growth rate [h-1]"].apply(float)
flux_df = fluxes_sheet.loc[~np.isnan(growth_rate), S.columns].applymap(float).T
flux_df.fillna(0.0, inplace=True)

# fill in the biomass reaction (based on the growth rate and biomass model)
# TODO: implement this properly, now we only use the default model (Schuetz)
biomass_df = pd.read_excel(INPUT_FNAME, sheet_name="e_coli_schuetz_biomass",
                           header=0, index_col=None)
S["BIOMASS"] = -biomass_df.iloc[:, 2:].T
S["BIOMASS"].fillna(0.0, inplace=True)
L["BIOMASS"] = 0.0
U["BIOMASS"] = 10.0

# calculate the relative fluxes
inv_glc_uptake = 1.0 / flux_df.iloc[1:9, :].sum(axis=0)
rel_flux_df = flux_df.multiply(inv_glc_uptake)

#%%
n_components = 3

decomposer = PCA(n_components)
W_PCA = decomposer.fit(rel_flux_df.T.values).components_.T

seed(2019)  # Helps in comparing between code versions
pmf = PMF(rel_flux_df.values, S.values, L, U, order=2,
          lambda_coefficient=0.5, sparse_coefficient=-1)
W_PMFA = pmf.solve(n_components=n_components)

#%% Report the variance explained and residuals for each method
fsampler = FluxSampler()
fig, axs = plt.subplots(2, 1, figsize=(10, 10))
for (W, name), ax in zip([(W_PCA, "PCA"), (W_PMFA, "PMFA")], axs.flat):
    explained_var = pmf.variance_explained(W)
    residuals_l1 = pmf.residuals(W, 1)
    residuals_l2 = pmf.residuals(W, 2)
    for j in range(n_components):
        cum_exp_var = explained_var[:j+1].sum()
        print(f"{name} component #{j+1}, "
              f"cumulative explained var = {cum_exp_var:.4g}, "
              f"residual l1 = {residuals_l1[j]:.4g}, "
              f"residual l2 = {residuals_l2[j]:.4g}, "
              f"||W||_l1 = {norm(W[:, j], ord=1):.4g}, "
              f"||W||_l2 = {norm(W[:, j], ord=2):.4g}")
    ax.set_title(name)
    components = pd.DataFrame(data=W, index=S.columns)
    components.plot.bar(ax=ax)
    for i in range(n_components):
        escher_builder = fsampler.display_in_escher(components[i])
        escher_builder.save_html(os.path.join(RESULT_DIR,
                                              f'{name}_component{i}.html'))
fig.savefig(os.path.join(RESULT_DIR, 'ecoli_pmfa_loadings.pdf'))

#%%
fig, axs = plt.subplots(2, 1, figsize=(10, 10))
for (W, name), ax in zip([(W_PCA, "PCA"), (W_PMFA, "PMFA")], axs.flat):
    proj_flux = rel_flux_df.T.dot(W)
    proj_flux = pd.merge(proj_flux, growth_rate,
                         left_index=True, right_index=True)
    proj_flux.plot.scatter(x=0, y=1, c=growth_rate.name, ax=ax,
                           cmap="viridis")
    ax.set_title(name)
fig.savefig(os.path.join(RESULT_DIR, 'ecoli_pmfa_growth_rate.pdf'))

#%%
for W, name in [(W_PCA, "PCA"), (W_PMFA, "PMFA")]:
    proj_flux = rel_flux_df.T.dot(W)
    proj_flux = pd.merge(proj_flux, meta_df,
                         left_index=True, right_index=True)

    proj_flux["wild_type"] = (proj_flux.mutation == "wild-type")
    categories = ["species", "condition", "original_source", "wild_type"]
    pairs = list(itertools.combinations(range(n_components), 2))
    fig, axs = plt.subplots(len(categories), len(pairs), figsize=(20, 25))

    for c, category in enumerate(categories):
        for ax, (i, j) in zip(axs[c, :].flat, pairs):
            labels = list(proj_flux[category].unique())
            palette = sns.color_palette("muted", n_colors=len(labels))
            for color, (label_k, data_k) in zip(palette,
                                                proj_flux.groupby(category)):
                plt.sca(ax)
                ax.scatter(data_k[i], data_k[j], label=label_k,
                           color=color)
                ax.set_xlabel(f"{name} #{i+1}")
                ax.set_ylabel(f"{name} #{j+1}")

            ax.legend(title=category, fontsize=5)
    fig.savefig(os.path.join(RESULT_DIR, f'ecoli_pmfa_{name}_scatter.pdf'))


