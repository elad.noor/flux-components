# MIT License
#
# Copyright (c) 2019 Elad Noor
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from typing import Tuple
import numpy as np
import pandas as pd
from cobra import Metabolite, Model, Reaction
from cobra.sampling import sample
from equilibrator_api import ureg
from cobamp.wrappers import KShortestEFMEnumeratorWrapper
from escher import Builder
from matplotlib.colors import rgb2hex
import seaborn as sns

from .settings import get_data
from .biomass_composition import BiomassComposition


class FluxSampler(object):
    DEFAULT_BIOMASS_MODEL = 'e_coli'
    DEFAULT_GROWTH_RATE = ureg.Measurement(0.8, 0, "1/hour")

    def __init__(self):
        self.stoich_df = get_data(sheet_name='reactions', index_col=0,
                                  header=0).fillna(0)
        self.meta_df = self.stoich_df.iloc[:, 0:6]
        self.stoich_df = self.stoich_df.iloc[:, 6:]

        # create the cobra model
        self.model = Model('E. coli')
        self.model.solver = 'cplex'

        self.model.add_metabolites(list(map(Metabolite, self.stoich_df)))
        self.model.add_reactions(list(map(Reaction, self.stoich_df.index)))

        for rxn, row in self.stoich_df.iterrows():
            r = self.model.reactions.get_by_id(rxn)
            r.lower_bound = self.meta_df.at[rxn, 'lb']
            r.upper_bound = self.meta_df.at[rxn, 'ub']
            r.add_metabolites(row.to_dict())

        # replace the biomass function with the organism specific one (which
        # is also growth-rate dependent)
        self.comp_df = BiomassComposition(
            FluxSampler.DEFAULT_BIOMASS_MODEL).GetComposition(
            FluxSampler.DEFAULT_GROWTH_RATE)
        self.comp_df = self.comp_df.apply(lambda d: d.nominal_value)

        # biomass is a placeholder reaction that we need to populate using
        # our biomass composition data
        bm_reaction = self.model.reactions.get_by_id('BIOMASS')

        # this is a "fake" metabolite that represents biomass
        biomass_c = Metabolite(compartment='c', id='biomass_c', name='Biomass')
        self.model.add_metabolites([biomass_c])
        bm_reaction.add_metabolites((-self.comp_df).to_dict())
        bm_reaction.add_metabolites({biomass_c: 1})

        # this is a "fake" reaction that gets rid of the biomass_c "metabolite"
        biomass_production = Reaction(id='EX_biomass_e',
                                      name='Biomass production',
                                      lower_bound=0, upper_bound=1000)
        biomass_production.add_metabolites({biomass_c: -1})
        self.model.add_reaction(biomass_production)

        # allow GLCxt uptake (the uptake rate itself is constrained by the
        # PTS flux which is set to 1)
        external_glc = Reaction("EXTERNAL_GLC")
        external_glc.add_metabolites({self.model.metabolites.GLCxt: 1})
        self.model.add_reaction(external_glc)

        self.model.objective = 'BIOMASS'

    @property
    def reactions(self):
        return self.model.reactions

    @property
    def metabolites(self):
        return self.model.metabolites

    def is_feasible(self) -> bool:
        status = self.model.optimize(objective_sense='maximize',
                                     raise_error=False)
        return status.status == 'optimal'

    @ureg.check(None, "1/[time]", "1/[time]")
    def set_biomass_range(
            self,
            lb: ureg.Quantity,
            ub: ureg.Quantity
    ) -> None:
        r = self.reactions.get_by_id('BIOMASS')
        r.lower_bound = (lb * ureg.Unit("hour")).magnitude
        r.upper_bound = (ub * ureg.Unit("hour")).magnitude
        assert self.is_feasible(), (f"Model is infeasible when "
                                    f"{r.lower_bound} < v_biomass < "
                                    f"{r.upper_bound}")

    def sample(
            self,
            n_samples: int = 100
    ) -> pd.DataFrame:
        sample_df = sample(self.model, n_samples)
        internal_reactions = self.meta_df[self.meta_df.external == 0].index
        return sample_df[internal_reactions]

    def multisample(
            self,
            n_samples: int = 100,
            n_bins: int = 10
    ) -> pd.DataFrame:
        """
            Arguments:
                n_samples - number of samples in each bin
                n_bins    - number of bins for the biomass constraint
            samples better by dividing the polytope into n_levels along
            the biomass axis. otherwise, the samples tend to have much lower
            biomass rate (since the polytope becomes very narrow in the
            high biomass rate area).
        """
        r_biomass = self.reactions.get_by_id('BIOMASS')
        lb = r_biomass.lower_bound
        ub = r_biomass.upper_bound

        status = self.model.optimize(objective_sense='maximize')
        if status.status != 'optimal':
            raise Exception(f"LP is {status}")

        max_bm = min(ub, status.objective_value)

        all_samples = []
        grid = np.linspace(lb, max_bm, num=n_bins + 1)
        for i in range(n_bins):
            r_biomass.lower_bound = grid[i]
            r_biomass.upper_bound = grid[i + 1]
            if not self.is_feasible():
                break
            s = self.sample(int(n_samples))
            all_samples.append(s)

        r_biomass.lower_bound = lb
        r_biomass.upper_bound = ub

        return pd.concat(all_samples).sort_values('BIOMASS')

    def fva_all_reactions(self) -> pd.DataFrame:
        """
            Find the minimum and maximum values in all elementary flux 
            directions.
            
            Returns:
                a DataFrame with the minimum and maximum values of each flux
        """
        fva_data = []
        for rxn in self.reactions:
            fva_data.append(self.fva({rxn.id: 1}))
        
        return pd.DataFrame(data=fva_data,
                            index=[rxn.id for rxn in self.reactions],
                            columns=['minimum', 'maximum'])

    def fva(self, direction: pd.Series) -> Tuple[float, float]:
        """
            Find the minimum and maximum values in a certain direction in 
            flux space.
            
            Arguments:
                direction - a Series with reactions as index and float
                            values as the coefficients
                            
            Returns:
                min, max
        """
        # make a dictionary where the keys are Reaction objects
        # and values are the coefficients
        obj = {self.reactions.get_by_id(r): c for r, c in direction.items()}

        temp_model = self.model.copy()
        temp_model.objective = obj

        status = temp_model.optimize(objective_sense='maximize')
        if status.status == 'infeasible':
            raise Exception('Infeasible LP, cannot run FVA')
        maxval = status.objective_value

        status = temp_model.optimize(objective_sense='minimize')
        if status.status == 'infeasible':
            raise Exception('Infeasible LP, cannot run FVA')
        minval = status.objective_value

        return minval, maxval

    def phenotypic_phase_plane(
            self, xdir: pd.Series, ydir: pd.Series, num: int = 100
    ) -> pd.DataFrame:
        """
            Draw a 2D phenotypic phase plane plot, i.e. a projection
            of the flux polytope on two given vectors
        """
        xmin, xmax = self.fva(xdir)

        yobj = {self.reactions.get_by_id(r): c for r, c in ydir.items()}

        temp_model = self.model.copy()
        temp_model.objective = yobj

        # add a 'fake' metabolite which will represent the x-direction
        # and add it with the proper coefficient to each of the reactions
        # involved in the x-direction.
        # also, add another sink reaction that we will use to fix the objective
        # value (i.e. the b value of the constraint in Ax = b)

        temp_model.add_metabolites([Metabolite('__dir_x')])
        for r, c in xdir.items():
            temp_model.reactions.get_by_id(r).add_metabolites({'__dir_x': c})

        r = Reaction('__dir_x_export')
        temp_model.add_reactions([r])
        r.add_metabolites({'__dir_x': -1})

        perimeter_up = []
        perimeter_down = []
        for x in np.linspace(xmin, xmax, num=num):
            r.lower_bound = x - 1e-3
            r.upper_bound = x + 1e-3
            min_y = temp_model.optimize(objective_sense='minimize').objective_value
            max_y = temp_model.optimize(objective_sense='maximize').objective_value
            perimeter_up += [(x, max_y)]
            perimeter_down += [(x, min_y)]

        perimeter = perimeter_up + list(reversed(perimeter_down)) + [perimeter_up[0]]
        return pd.DataFrame(perimeter)

    def get_efms(self, stop_criteria: int = 26) -> pd.DataFrame:
        ksefm = KShortestEFMEnumeratorWrapper(
            model=self.model,
            non_consumed=[],
            consumed=['GLCxt'],
            produced=['biomass_c'],
            algorithm_type=KShortestEFMEnumeratorWrapper.ALGORITHM_TYPE_POPULATE,
            stop_criteria=stop_criteria
        )
        enumerator = ksefm.get_enumerator()
        efm_list = [efm for sublist in enumerator for efm in sublist]
        efm_df = pd.DataFrame.from_dict(efm_list).fillna(0)
        efm_df = efm_df.multiply(1.0 / efm_df.im_glc, axis='index')
        return efm_df

    def display_in_escher(
            self,
            fluxes: pd.Series,
            map_name: str = "iJO1366.Central metabolism",
            max_flux: float = .0
    ) -> Builder:
        """Display flux results using Escher.

        :param fluxes: a pandas Series with the flux data
        :param map_name: the name of the Escher map to use
        :return:
        """
        # first, we need to map the reaction names used in our core model to
        # the names used in Escher (i.e. BiGG database IDs)
        name_df = self.meta_df.escher_name.str.split(',', expand=True)
        name_df = name_df.reset_index().melt(id_vars="short name",
                                             value_name="escher_name")
        name_df = name_df[~pd.isnull(name_df.escher_name)]
        name_df = name_df[["short name", "escher_name"]]

        # create a dictionary mapping reaction names (as they appear in the
        # Escher model) to fluxes
        escher_fluxes = name_df.join(fluxes.to_frame(),
                                     on="short name",
                                     how="right").groupby("escher_name").sum()
        reaction_data = escher_fluxes.iloc[:, 0].to_dict()

        if max_flux == 0:
            max_flux = fluxes.max()
        n = 20
        reaction_scale = [
            {"type": "value", "value": x, "color": rgb2hex(c), "size": s}
            for x, c, s in zip(np.linspace(0, max_flux, n),
                               sns.color_palette("Blues", n_colors=n),
                               np.linspace(20, 40, n))
        ]

        return Builder(
            map_name=map_name,
            model=self.model,
            reaction_data=reaction_data,
            reaction_styles=['color', 'size', 'abs', 'text'],
            reaction_scale=reaction_scale,
            reaction_no_data_color="#eeeeee"
        )

    def CalculateExplainedVariance(self, v, draw_figure=False):
        rxn_ignore = ['BIOMASS', 'REDOX', 'RESPIRATION']
        X = self.rel_flux_df.applymap(nominal_value)
        X = X.drop(rxn_ignore, axis=1)
        C = np.cov(X.T)

        v = v[X.columns]

        explained = v.dot(C.dot(v)) / (np.trace(C) * v.dot(v))

        if draw_figure:
            fig, ax = plt.subplots(1, 1, figsize=(6, 4))
            v.plot.bar(ax=ax)
            ax.set_title('explains %.1f%% of the variance' % (100 * explained))
            return fig
        else:
            return explained

    def AnalyzeEFM(self):
        """
            Find the EFM that explains the highest percent of the total
            flux variance in the measured flux matrix
        """

        from efm_analysis import ElementaryFluxModeAnalysis
        efma = ElementaryFluxModeAnalysis()
        efma.SetBiomassComposition('e_coli', 0.8)
        efm_df = efma.GetEFMs()

        rxn_ignore = ['BIOMASS', 'REDOX', 'RESPIRATION']
        X = self.rel_flux_df.applymap(nominal_value)
        X = X.drop(rxn_ignore, axis=1)
        C = np.cov(X.T)

        # keep only the reactions that are in the measured flux dataset
        # (and also are not one of the ignored fluxes)
        # and find the EFM with the highest explained variance
        expl = {}
        for i, v in efm_df[X.columns].iterrows():
            expl[i] = v.dot(C.dot(v)) / (np.trace(C) * v.dot(v))
        expl = pd.Series(expl)

        best_efm = efm_df.loc[expl.idxmax(), :]
        return best_efm
