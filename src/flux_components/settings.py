"""The aim of this project is to collect as many measurements of steady-state
fluxes as posslbe (i.e. using 13C flux analysis) and explore the phenotypic
space created by them."""
# MIT License
#
# Copyright (c) 2019 Elad Noor
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import xlrd
import pandas as pd
from typing import Union

from importlib_resources import open_binary
import flux_components.data
from equilibrator_api import (Q_, ComponentContribution, R, Reaction, ccache,
                              default_T, ureg, parse_reaction_formula, Compound)

def get_data(
        sheet_name: str,
        header: Union[int, None] = 0,
        index_col: Union[int, None] = None,
        skiprows: Union[int, None] = None
) -> pd.DataFrame:
    with open_binary(flux_components.data, "meta_analysis.xls") as fp:

        # if sheet_name not in xlrd.open_workbook(fp).sheet_names():
        #     raise KeyError(f"There is no sheet called {sheet_name} in the "
        #                    f"data file.")
        df = pd.read_excel(
            fp, sheet_name=sheet_name, index_col=index_col, header=header,
            skiprows=skiprows)
    return df

_relpath = os.path.dirname(os.path.realpath(__file__))
RESULT_DIR = os.path.join(_relpath, "..", "..", "res")

