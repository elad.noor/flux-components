"""Flux Compenent Analysis"""
# MIT License
#
# Copyright (c) 2019 Elad Noor
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.decomposition import PCA
from typing import List
from uncertainties import nominal_value, std_dev

from .settings import (
    ureg, Q_, get_data, ComponentContribution, ccache, R, default_T,
    parse_reaction_formula)
from .biomass_composition import BiomassComposition
from .flux_sampler import FluxSampler


class FluxComponentAnalysis(object):
    N_SAMPLES = 10000
    N_BINS = 10
    FLUX_UNITS = ureg.Unit("mmol/gram/hour")
    E_DISS_UNITS = ureg.Unit("watt/gram")
    
    BIOMASS_HEAT_OF_COMBUSTION = ureg.Measurement(20.0, 0.0, "kJ/gram")
    EPSILON_FLUX = ureg.Measurement(1e-9, 0.0, FLUX_UNITS)

    def __init__(self):

        # read the stoichiometric data from the sheet "reactions"
        self.stoich_df = get_data(sheet_name='reactions',
                                  index_col=0, header=0).fillna(0)
        self.stoich_df = self.stoich_df[self.stoich_df.external == 0]
        self.stoich_df = self.stoich_df.iloc[:, 6:]

        # read the flux data from the sheet "fluxes"
        df = get_data(sheet_name='fluxes', index_col=None, header=None,
                      skiprows=1)

        NUM_REACTIONS = self.stoich_df.shape[0]
        NUM_META_COLUMNS = df.shape[1] - 2 * NUM_REACTIONS

        # this code breaks the big table into three managable ones.
        # it is not very elegant, but Pandas couldn't handle my MultiIndex
        # header for some reason
        self.meta_df = df.iloc[1:, 0:NUM_META_COLUMNS].copy()
        self.meta_df.index = self.meta_df.index.map(int)
        self.meta_df.columns = df.iloc[0, 0:NUM_META_COLUMNS]
        self.meta_df.index.name = 'serial_number'

        self.meta_df["growth_rate"] = ureg.Measurement(np.nan, np.nan, "1/hour")
        for i, row in self.meta_df.iterrows():
            try:
                gr = float(row["growth rate [h-1]"])
                gr_std = float(row["growth rate error [h-1]"])
                self.meta_df.at[i, "growth_rate"] = ureg.Measurement(
                    gr, gr_std, "1/hour")
            except TypeError:
                continue
        self.meta_df.drop(['growth rate [h-1]',
                           'growth rate error [h-1]'], axis=1, inplace=True)

        reaction_names = df.iloc[0, NUM_META_COLUMNS:(NUM_META_COLUMNS + NUM_REACTIONS)]
        self.flux_df = pd.DataFrame(index=self.meta_df.index,
                                    columns=reaction_names)
        self.flux_df = self.flux_df.fillna(
            ureg.Measurement(0, 0, FluxComponentAnalysis.FLUX_UNITS))
        self.flux_df.columns.name = "reaction"

        for i, snum in enumerate(self.flux_df.index):
            for j, rxn in enumerate(self.flux_df.columns):
                f = float(df.iat[1 + i, NUM_META_COLUMNS + j])
                e = float(df.iat[1 + i, NUM_META_COLUMNS + NUM_REACTIONS + j])
                f = 0 if np.isnan(f) else f
                e = 0 if np.isnan(e) else e
                if e < 0:
                    raise ValueError(f"One of the fluxes has a negative "
                                     f"standard deviation: {snum} {rxn}")
                self.flux_df.at[snum, rxn] = ureg.Measurement(
                    f, e, FluxComponentAnalysis.FLUX_UNITS)

        # read the "degrees of freedom" information
        dof_df = get_data(sheet_name='degrees_of_freedom',
                          index_col=0, header=0, skiprows=1)
        dof_df = dof_df.fillna(0)

        n_dofs = int(dof_df.shape[1] / 2)
        numerators = self.flux_df @ dof_df.iloc[:, 0:n_dofs]
        denominators = self.flux_df @ dof_df.iloc[:, n_dofs:]
        denominators.columns = numerators.columns

        self.dof_df = pd.DataFrame(index=numerators.index,
                                   columns=numerators.columns,
                                   dtype=object)

        for dof in numerators.columns:
            for snum in numerators.index:
                if denominators.at[snum, dof] == 0:
                    if numerators.at[snum, dof] == 0:
                        self.dof_df.at[snum, dof] = ureg.Measurement(0, 0)
                    else:
                        self.dof_df.at[snum, dof] = np.nan
                else:
                    self.dof_df.at[snum, dof] = (numerators.at[snum, dof] /
                                                 denominators.at[snum, dof])

        # find the rows where there is a positive glucose uptake flux and
        # normalize them by the glucose uptake (i.e. the PTS flux)
        has_glc_uptake = (
                self.flux_df["PTS"] >
                ureg.Measurement(1e-5, 0, FluxComponentAnalysis.FLUX_UNITS)
        )
        _df = self.flux_df[has_glc_uptake]
        self.rel_flux_df = _df.divide(_df["PTS"], axis="rows")

        # read the thermodynamic info
        self.thermo_df = get_data(sheet_name='thermodynamics',
                                  index_col=0, header=0)

        equilibrator = ComponentContribution(
            p_h=Q_(7.0), ionic_strength=Q_("0.1M")
        )
        conc_df = get_data(sheet_name='concentrations', header=0)
        conc_df["compound"] = conc_df.compound_id.apply(ccache.get_compound)
        conc_df.set_index("compound", inplace=True)

        conc_df.min_concentration = conc_df.min_concentration.apply(Q_)
        conc_df.max_concentration = conc_df.max_concentration.apply(Q_)

        ln_conc_list = []
        conc_df["ln_conc"] = ureg.Measurement(0, 0)
        for row in conc_df.itertuples():
            if row.max_concentration < row.min_concentration:
                raise ValueError(f"Maximum concentration must be larger than "
                                 f"minimum, check {row.metabolite}")
            ln_conc_max = np.log(row.max_concentration / Q_("1 M"))
            ln_conc_min = np.log(row.min_concentration / Q_("1 M"))
            ln_conc_mean = (ln_conc_max + ln_conc_min) / 2.0
            ln_conc_std = (ln_conc_max - ln_conc_min) / 2.0
            ln_conc_list.append(ureg.Measurement(ln_conc_mean, ln_conc_std))
        conc_df["ln_conc"] = ln_conc_list

        def formula_to_dg_prime(
                formula: str,
                oxidation: bool = False
        ) -> ureg.Measurement:
            reaction = parse_reaction_formula(formula)
            if oxidation:
                reaction = reaction.balance_by_oxidation()

            dg_prime = equilibrator.standard_dg_prime(reaction)
            for compound, coeff in reaction.sparse.items():
                if ccache.is_water(compound) or ccache.is_proton(compound):
                    continue
                if compound in conc_df.index:
                    ln_conc = conc_df.at[compound, "ln_conc"]
                    dg_prime += coeff * R * default_T * ln_conc
            return dg_prime

        self.thermo_df["formation_energy"] = \
            self.thermo_df.formula.apply(
                lambda formula: formula_to_dg_prime(formula, oxidation=False)
            )

        self.thermo_df["combustion_energy"] = \
            self.thermo_df.formula.apply(
                lambda formula: formula_to_dg_prime(formula, oxidation=True)
            )

        self.conc_df = conc_df

        # calculate the energy dissipation rates
        flux_imbalance = self.flux_df.dot(self.stoich_df)
        flux_imbalance = flux_imbalance.reset_index().melt(
            id_vars='serial_number', var_name='metabolite', value_name='flux'
        )
        flux_imbalance = flux_imbalance.join(self.thermo_df,
                                             how='inner', on='metabolite')

        flux_imbalance["process"] = None
        for row in flux_imbalance.itertuples():
            if row.precursor == 1:
                process = "precursor_heat_of_combustion"
            elif row.external != 1:
                process = None
            elif row.flux < -FluxComponentAnalysis.EPSILON_FLUX:
                process = 'import_heat_of_combustion'
            elif row.flux > FluxComponentAnalysis.EPSILON_FLUX:
                process = 'fermentation_heat_of_combustion'
            flux_imbalance.loc[row.Index, "process"] = process

        # multiply the flux by the energy difference to get the dissipation
        # rate
        flux_imbalance["energy_dissipation_rate"] = \
            flux_imbalance.flux * flux_imbalance.formation_energy
        flux_imbalance["heat_of_combustion_rate"] = \
            -flux_imbalance.flux * flux_imbalance.combustion_energy

        # calculate the totals of the energy dissipation rates for each
        # process and for the overall (using formation energies)
        energy_dict = {}
        for sn, df1 in flux_imbalance.groupby("serial_number"):
            energy_dict[(sn, "energy_dissipation_rate")] = \
                sum(df1.energy_dissipation_rate)
            for process, df2 in df1.groupby("process"):
                energy_dict[(sn, process)] = sum(df2.heat_of_combustion_rate)

        energy_df = pd.Series(energy_dict).reset_index()
        energy_df.columns = ["serial_number", "process", "energy"]
        energy_df.energy.apply(FluxComponentAnalysis.E_DISS_UNITS.from_)
        energy_df = energy_df.pivot("serial_number", "process", "energy")

        self.meta_df = self.meta_df.join(energy_df, on="serial_number")

        self.meta_df["biomass_heat_of_combustion_rate"] = \
            self.meta_df.growth_rate.apply(lambda x:
                FluxComponentAnalysis.E_DISS_UNITS.from_(
                    x*self.BIOMASS_HEAT_OF_COMBUSTION)
            )

    @staticmethod
    def PlotGroups(df, groupbycols, x_col, y_col,
                   x_unit=None, y_unit=None,
                   show_xerr=True, show_yerr=True,
                   zorder=1):
        fig, axs = plt.subplots(2, 2, figsize=(14, 12),
                                sharex=True, sharey=True)
        
        # make a copy of the DataFrame where the x and y values are unitless
        df = df.copy()
        if x_unit is not None:
            df[x_col] = df[x_col].apply(x_unit.m_from)
        if y_unit is not None:
            df[y_col] = df[y_col].apply(y_unit.m_from)
        for ax, groupbycol in zip(axs.flat, groupbycols):
            groups = df.groupby(groupbycol)
            if len(groups) < 8:
                cmap = sns.color_palette('muted', n_colors=len(groups))
            else:
                cmap = sns.color_palette('tab20', n_colors=len(groups))

            for i, (gr_name, gr) in enumerate(groups):
                ax.errorbar(
                        gr[x_col].apply(nominal_value),
                        gr[y_col].apply(nominal_value),
                        xerr=(gr[x_col].apply(std_dev) if show_xerr else None),
                        yerr=(gr[y_col].apply(std_dev) if show_yerr else None),
                        c=cmap[i], label=gr_name, zorder=zorder, fmt='.')
            ax.legend(fontsize=7, loc='best', title=groupbycol)

            if x_unit is not None:
                ax.set_xlabel(f"{x_col} [{x_unit}]")
            else:
                ax.set_xlabel(f"{x_col}")

            if y_unit is not None:
                ax.set_ylabel(f"{y_col} [{y_unit}]")
            else:
                ax.set_xlabel(f"{y_col}")
        return fig, axs

    def TestBiomassFunction(self, sources: List[str] = None) -> plt.Figure:
        """
            Calculate the growth-rate depedendant biomass functions
            and see if they properly balance all the other fluxes
        """
        biomass_series_list = []
        for row in self.meta_df.itertuples():
            try:
                bmc = BiomassComposition(row.biomass_model)
            except ValueError as e:
                logging.error(e)
                continue
            consume = bmc.GetPrecursorBiomassFluxes(row.growth_rate)
            consume.name = row.Index
            biomass_series_list.append(consume)

        consumption_df = pd.DataFrame(biomass_series_list)
        consumption_df.index.name = "serial_number"

        # Calculate all the production rates by multiplying the flux table by
        # the stoichiometric matrix. Since the biomass function is not part
        # of this matrix, all precursors will have imbalances that should match
        # the biomass consumption rates that we just calculated
        production_df = self.flux_df.fillna(
            ureg.Measurement(0, 0, FluxComponentAnalysis.FLUX_UNITS)
        ).dot(self.stoich_df)

        a = consumption_df.reset_index().melt(
                id_vars=['serial_number'],
                value_vars=consumption_df.columns,
                var_name='precursor',
                value_name='consumption')

        b = production_df.reset_index().melt(
                id_vars=['serial_number'],
                value_vars=production_df.columns,
                var_name='precursor',
                value_name='production')

        compare_df = pd.merge(a, b,
                              on=['serial_number', 'precursor'],
                              how='inner')
        compare_df = compare_df.join(self.meta_df, on='serial_number')

        # skip flux samples that only have relative fluxes
        # TODO: is this necessary actually?
        compare_df = compare_df[compare_df['is_absolute_flux'] == 1]

        # remove ATP since we do not have maitenance in the model
        compare_df = compare_df[compare_df.precursor != 'ATP']

        # plot the imbalances of each metabolite versus the biomass requirement
        # select a column to color the samples by
        if sources:
            fig, axs = FluxComponentAnalysis.PlotGroups(
                compare_df[compare_df.original_source.isin(sources)],
                ['biomass_model', 'precursor', 'mutation', 'strain'],
                x_col='consumption', y_col='production',
                x_unit=FluxComponentAnalysis.FLUX_UNITS,
                y_unit=FluxComponentAnalysis.FLUX_UNITS,
                show_yerr=False)
        else:
            fig, axs = FluxComponentAnalysis.PlotGroups(
                compare_df,
                ['biomass_model', 'precursor', 'original_source', 'strain'],
                x_col='consumption', y_col='production',
                x_unit=FluxComponentAnalysis.FLUX_UNITS,
                y_unit=FluxComponentAnalysis.FLUX_UNITS,
                show_yerr=False)

        for ax in axs.flat:
            ax.set_xscale('log')
            ax.set_yscale('log')
            ax.plot([1e-3, 1e2], [1e-2, 1e3], c=(0.9, 0.9, 0.9), zorder=1)
            ax.plot([1e-3, 1e2], [1e-3, 1e2], c=(0.7, 0.7, 0.7), zorder=1)
            ax.plot([1e-3, 1e2], [1e-4, 1e1], c=(0.9, 0.9, 0.9), zorder=1)
        return fig

    def FluxPrincipalComponentAnalysis(self):
        # list of reactions to ignore in the PCA itself, since some of the
        # samples have no data for them. We will still include them in the
        # final projected dataframe
        rxn_ignore = ['biomass', 'redox', 'respiration']

        growth_rates = self.rel_flux_df['biomass'].apply(
            lambda x: ureg.Quantity(x.nominal_value, "1/hour"))

        f_meas = self.rel_flux_df.drop(rxn_ignore, axis=1).applymap(
            nominal_value)

        fsampler = FluxSampler()
        fsampler.set_biomass_range(growth_rates.min(), growth_rates.max())
        f_smpl = fsampler.multisample(
                FluxComponentAnalysis.N_SAMPLES / FluxComponentAnalysis.N_BINS,
                n_bins=self.N_BINS)

        n_components = 4
        decomposition = PCA(n_components)

        # convert fluxes to relative flux (by normalizing glucose uptake to 1)
        # decomposition.fit(f_meas)
        decomposition.fit(f_smpl[f_meas.columns])

        # plot the loadings of the first 3 PCs
        col_names = []
        for i, var in enumerate(decomposition.explained_variance_ratio_):
            col_names.append(f"PCA #{i} ({var * 100:.1f}%)")
        decomp_df = pd.DataFrame(decomposition.components_.T,
                                 index=f_meas.columns,
                                 columns=col_names)

        for r in rxn_ignore:
            decomp_df.loc[r, r] = 1
        decomp_df = decomp_df.fillna(0)

        fig, axs = plt.subplots(n_components, 1, figsize=(10, n_components * 3))
        for i, ax in enumerate(axs.flat):
            decomp_df[col_names[i]].plot.bar(ax=ax)
            ax.set_title(col_names[i])
            if i < n_components - 1:
                ax.get_xaxis().set_ticks([])
                ax.set_xlabel('')

        fig.savefig('../res/pca_loadings.pdf')

        proj_meas = self.rel_flux_df.dot(decomp_df)
        proj_meas = proj_meas.reset_index().join(self.meta_df, on='serial_number')

        sources = proj_meas['original_source'].unique()
        cmap = dict(zip(sources, sns.color_palette("muted", n_colors=len(sources))))

        # plot a sorted bar plot of the measured data according to the 4 PCs 
        fig, axs = plt.subplots(n_components, 1, figsize=(20, n_components * 3))
        for i, ax in enumerate(axs.flat):
            _data = proj_meas.copy()
            _data.sort_values(col_names[i], inplace=True)
            _data[col_names[i]].apply(nominal_value).plot.bar(
                ax=ax, color=_data['original_source'].apply(cmap.get))
            ax.set_ylabel(col_names[i])
            ax.get_xaxis().set_ticks([])
            ax.set_xlabel('')

        fig.savefig('../res/pca_sorted_bars.pdf')

        # plot a scatter plot of the first two PCs of the measured data
        # and overlay with a 2D KDE plot of the sampled data
        x_col, y_col = map(col_names.__getitem__, [0, 1])

        fig, axs = FluxComponentAnalysis.PlotGroups(
            proj_meas, ['species', 'strain', 'original_source', 'condition'],
            x_col=x_col, y_col=y_col)

        proj_smpl = f_smpl.dot(decomp_df)
        ppp = fsampler.phenotypic_phase_plane(decomp_df[x_col],
                                              decomp_df[y_col])
        ppp.columns = [x_col, y_col]
        for ax in axs.flat:
            sns.kdeplot(proj_smpl[x_col], proj_smpl[y_col], cmap='viridis',
                        zorder=0, ax=ax, alpha=0.2)
            # ax.scatter(proj_smpl[x_col], proj_smpl[y_col],
            #           zorder=0, color=(0.9, 0.9, 0.9), alpha=0.5,
            #           label='FBA samples')
            ppp.plot(x=x_col, y=y_col, ax=ax, style=':',
                     zorder=0, color=(0.8, 0.8, 0.8),
                     label='flux polytope')
            ax.legend(loc='center right')

        fig.savefig('../res/pca_projections.pdf')

        # second figure - each PC versus biomass

        fig, axs = plt.subplots(2, 2, figsize=(14, 12))
        # plt.subplots_adjust(wspace=0.3, hspace=0.25)

        groups = proj_meas.groupby('original_source')
        for i, ax in enumerate(axs.flat):
            x_col = col_names[i]
            y_col = 'biomass'
            for j, (gr_name, gr) in enumerate(groups):
                ax.errorbar(gr[x_col].apply(nominal_value),
                            gr[y_col].apply(nominal_value),
                            xerr=gr[x_col].apply(std_dev),
                            yerr=gr[y_col].apply(std_dev),
                            c=cmap[gr_name], label=gr_name, zorder=2, fmt='.')
            ax.set_xlabel(x_col, fontsize=10)
            ax.set_ylabel(y_col, fontsize=10)
            sns.kdeplot(proj_smpl[x_col], proj_smpl[y_col], cmap='viridis',
                        zorder=0, ax=ax, alpha=0.2)
            #            ax.scatter(proj_smpl[x_col], proj_smpl[y_col],
            #                       zorder=0, color=(0.9, 0.9, 0.9), alpha=0.5,
            #                       label='FBA samples')
            ppp = fsampler.phenotypic_phase_plane(decomp_df[x_col],
                                                  decomp_df[y_col])
            ppp.columns = [x_col, y_col]
            ppp.plot(x=x_col, y=y_col, ax=ax, style=':',
                     zorder=0, color=(0.8, 0.8, 0.8),
                     label='flux polytope')
            ax.legend(fontsize=7, loc='best')

        fig.savefig('../res/pca_yield.pdf')

    def PlotCorrelations(self):
        """
            Plot the flux and the sample correlation matrices
        """
        # remove flux columns that are entirely 0 (this crashes the corrcoef function)
        cols_to_keep = self.rel_flux_df.columns[(self.rel_flux_df != 0).any(0)]

        # order the samples according to their source
        src_ord = self.meta_df['original_source'].sort_values()
        V_smp = self.rel_flux_df.loc[src_ord.index, cols_to_keep]

        # numpy.corrcoef only works with floats, so we must throw away the
        # uncertainty values:
        V_smp = V_smp.applymap(nominal_value)

        V_flux = V_smp.transpose()

        # drop zero columns from V
        corrmatrix = pd.DataFrame(data=np.corrcoef(V_flux),
                                  columns=V_flux.index,
                                  index=V_flux.index)
        fig, ax = plt.subplots(1, 1, figsize=(10, 8), sharey=True)
        sns.heatmap(corrmatrix, ax=ax)
        fig.savefig('../res/corr_reactions.pdf')

        # drop zero columns from V
        corrmatrix = pd.DataFrame(data=np.corrcoef(V_smp),
                                  columns=src_ord,
                                  index=src_ord)
        fig, ax = plt.subplots(1, 1, figsize=(17, 15), sharey=True)
        sns.heatmap(corrmatrix, ax=ax)
        fig.savefig('../res/corr_samples.pdf')

    def AnalyseDegreesOfFreedom2(self, dim1, dim2):
        dof_meta_df = self.dof_df.reset_index().join(self.meta_df, on='serial_number')

        fig, axs = plt.subplots(2, 2, figsize=(10, 10), sharex=True, sharey=True)
        for ax, groupbycol, pal in zip(axs.flat[0:4],
                                       ['species', 'strain', 'original_source', 'condition'],
                                       ["tab20", "tab20", "muted", "muted"]):
            groups = dof_meta_df.groupby(groupbycol)
            cmap = sns.color_palette(pal, n_colors=len(groups))

            for i, (gr_name, gr) in enumerate(groups):
                ax.errorbar(gr[dim1].apply(nominal_value),
                            gr[dim2].apply(nominal_value),
                            xerr=gr[dim1].apply(std_dev),
                            yerr=gr[dim2].apply(std_dev),
                            c=cmap[i], label=gr_name, zorder=2, fmt='.')

            ax.legend(fontsize=7, loc='best', title=groupbycol)
        axs[0, 0].set_ylabel(dim2, fontsize=12)
        axs[1, 0].set_ylabel(dim2, fontsize=12)
        axs[1, 0].set_xlabel(dim1, fontsize=12)
        axs[1, 1].set_xlabel(dim1, fontsize=12)
        fig.savefig('../res/dof.pdf')

    def AnalyseDegreesOfFreedom(self):
        sources = self.meta_df['original_source'].unique()
        cmap = dict(zip(sources, sns.color_palette("muted", n_colors=len(sources))))

        Nd = self.dof_df.shape[1]
        # plot a histogram for each DOF
        fig, axs = plt.subplots(Nd, 1, figsize=(20, Nd * 3))
        for (col, ser), ax in zip(self.dof_df.T.iterrows(), axs.flat):
            ser = ser.apply(nominal_value).sort_values()
            color = self.meta_df.loc[ser.index, 'original_source'].apply(cmap.get)
            ser.plot.bar(ax=ax, color=color)
            ax.set_ylabel(col)

        fig.savefig('../res/dof_sorted_bars.pdf')

    #        fig, axs = plt.subplots(2, 2, figsize=(10, 10), sharex=True, sharey=True)
    #        for ax, groupbycol, pal in zip(axs.flat[0:4],
    #                                       ['species', 'strain', 'original_source', 'condition'],
    #                                       ["tab20", "tab20", "muted", "muted"]):
    #            groups = dof_meta_df.groupby(groupbycol)
    #            cmap = sns.color_palette(pal, n_colors=len(groups))
    #
    #            for i, (gr_name, gr) in enumerate(groups):
    #                ax.errorbar(gr[dim1].apply(F_NOM),
    #                            gr[dim2].apply(F_NOM),
    #                            xerr=gr[dim1].apply(F_STD),
    #                            yerr=gr[dim2].apply(F_STD),
    #                            c=cmap[i], label=gr_name, zorder=2, fmt='.')
    #
    #            ax.legend(fontsize=7, loc='best', title=groupbycol)
    #        axs[0, 0].set_ylabel(dim2, fontsize=12)
    #        axs[1, 0].set_ylabel(dim2, fontsize=12)
    #        axs[1, 0].set_xlabel(dim1, fontsize=12)
    #        axs[1, 1].set_xlabel(dim1, fontsize=12)
    #        fig.savefig('../res/dof.pdf')

    def CalculateExplainedVariance(self, v, draw_figure=False):
        rxn_ignore = ['biomass', 'redox', 'respiration']
        X = self.rel_flux_df.applymap(nominal_value)
        X = X.drop(rxn_ignore, axis=1)
        C = np.cov(X.T)

        v = v[X.columns]

        explained = v.dot(C.dot(v)) / (np.trace(C) * v.dot(v))

        if draw_figure:
            fig, ax = plt.subplots(1, 1, figsize=(6, 4))
            v.plot.bar(ax=ax)
            ax.set_title('explains %.1f%% of the variance' % (100 * explained))
            return fig
        else:
            return explained

    def AnalyzeEFM(self):
        """
            Find the EFM that explains the highest percent of the total
            flux variance in the measured flux matrix
        """

        from efm_analysis import ElementaryFluxModeAnalysis
        efma = ElementaryFluxModeAnalysis()
        efma.SetBiomassComposition('e_coli', 0.8)
        efm_df = efma.GetEFMs()

        rxn_ignore = ['biomass', 'redox', 'respiration']
        X = self.rel_flux_df.applymap(nominal_value)
        X = X.drop(rxn_ignore, axis=1)
        C = np.cov(X.T)

        # keep only the reactions that are in the measured flux dataset
        # (and also are not one of the ignored fluxes)
        # and find the EFM with the highest explained variance
        expl = {}
        for i, v in efm_df[X.columns].iterrows():
            expl[i] = v.dot(C.dot(v)) / (np.trace(C) * v.dot(v))
        expl = pd.Series(expl)

        best_efm = efm_df.loc[expl.idxmax(), :]
        return best_efm


if __name__ == '__main__':
    fca = FluxComponentAnalysis()
    #
    fig1 = fca.TestBiomassFunction()
    fig1.savefig("../res/test_biomass.pdf")

    fca.FluxPrincipalComponentAnalysis()
    fca.PlotCorrelations()
    fca.AnalyseDegreesOfFreedom()
    best_efm = fca.AnalyzeEFM()
    fca.CalculateExplainedVariance(best_efm, draw_figure=True)

    # it looks like the EFM that we chose is creating biomass from nothing.
    # therefore, a closer look is necessary

    #
    rxn_ignore = ['biomass', 'redox', 'respiration']
    X = fca.rel_flux_df.applymap(nominal_value)
    X = X.drop(rxn_ignore, axis=1)
    decomposition = PCA(X.shape[1])
    decomposition.fit(X)
    pca1 = pd.Series(decomposition.components_[0], index=X.columns)
    fca.CalculateExplainedVariance(pca1, draw_figure=True)

    #
    flux_with_meta = fca.rel_flux_df.join(fca.meta_df)
    fca.PlotGroups(flux_with_meta,
                   ['biomass_model', 'species', 'original_source', 'strain'],
                    'pgi', 'zwf_pgl')

    fca.PlotGroups(flux_with_meta,
                   ['biomass_model', 'species', 'original_source', 'strain'],
                    'biomass', 'zwf_pgl')
    
    C = np.cov(X.T)

    #
    v = pd.Series(index=X.columns, dtype=float).fillna(0.0)
    #v['glc_uptake'] = -1
    v['zwf_pgl'] = -1
    v['zmo_zwf'] = -1
    v['pgi'] = 1
    fca.CalculateExplainedVariance(v, draw_figure=True)

