#!/usr/bin/env python3
# MIT License
#
# Copyright (c) 2019 Elad Noor
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from uncertainties import nominal_value, std_dev

from .settings import ComponentContribution, Reaction, RESULT_DIR
from .flux_component_analysis import FluxComponentAnalysis

fca = FluxComponentAnalysis()


equilibrator = ComponentContribution(pH=7.0, ionic_strength=0.1)
merged_df = fca.meta_df.join(fca.flux_df[['pts']])

# %%
df = merged_df[(merged_df['is_absolute_flux'] == True) &
               (merged_df['Biomass Model'] == 'e_coli') &
               (merged_df['species'] == 'E. coli') &
               (merged_df['pts'] > 0) &
               (merged_df['mutation'] == 'wild-type')]

fig, axs = plt.subplots(2, 2, figsize=(15, 15))
ax = axs[0, 0]
# x = 'pts'; xlabel = 'glucose uptake rate [mmol gCDW$^{-1}$ h$^{-1}$]'
x = 'growth rate [h-1]';
xlabel = 'growth rate [h-1]'
y = '$g^{diss}$ [kJ gCDW$^{-1}$ h$^{-1}$]'

ax.set_prop_cycle(color=sns.color_palette('Paired', n_colors=11))
for gr, gr_df in df.groupby('strain'):
    ax.errorbar(gr_df[x].apply(nominal_value),
                np.abs(gr_df[y].apply(nominal_value)),
                xerr=(gr_df[x].apply(std_dev)),
                yerr=(gr_df[y].apply(std_dev)),
                fmt='o', linewidth=0.5,
                markersize=10, label=gr)

ax.set_xlim(0, None)
ax.set_ylim(0, None)
ax.set_xlabel(xlabel)
ax.set_ylabel(y)
ax.legend()

ax = axs[0, 1]
ax.set_prop_cycle(color=sns.color_palette('deep', n_colors=4))

x = 'pts'
for label in ['biomass $h^{comb}$', 'fermentation $h^{comb}$',
              'precursor $h^{comb}$', '$g^{diss}$']:
    y = label + ' [kJ gCDW$^{-1}$ h$^{-1}$]'
    ax.errorbar(df[x].apply(nominal_value),
                np.abs(df[y].apply(nominal_value)),
                xerr=(df[x].apply(std_dev)),
                yerr=(df[y].apply(std_dev)),
                fmt='.', linewidth=0.5, label=label, zorder=2,
                markersize=10)

ax.set_xlim(0, None)
ax.set_ylim(0, None)
ax.set_xlabel('glucose uptake rate [mmol gCDW$^{-1}$ h$^{-1}$]')
ax.set_ylabel('kJ gCDW$^{-1}$ h$^{-1}$')
_, xmax = ax.axes.get_xlim()

# draw the line representing several extreme strategies
strategies = {'glucose heat of combustion': 'C00031 + 6 C00007 = 6 C00011 + 6 C00001',
              'aero. acetate ferm.': 'C00031 + 2 C00007 = 2 C00011 + 2 C00001 + 2 C00033',
              'anaero. lactate ferm.': 'C00031 = 2 C00186',
              }

ax.set_prop_cycle(color=[(0.8, 0.6, 0.8), (0.6, 0.8, 0.8), (0.8, 0.8, 0.6)])

for label, formula in strategies.items():
    reaction = Reaction.parse_formula(formula)

    dG0_prime, dG0_uncertainty = equilibrator.dG0_prime(reaction)
    ax.fill_between([0, xmax],
                    y1=[0, xmax * (-dG0_prime - 1.96 * dG0_uncertainty) / 1000],
                    y2=[0, xmax * (-dG0_prime + 1.96 * dG0_uncertainty) / 1000],
                    label=label, zorder=1)

ax.legend(loc='best');

df.to_csv(os.path.join(RESULT_DIR, 'gdiss.csv'))

# plot the dissipation as stacked bar plots

ax = axs[1, 0]
ind = df['growth rate [h-1]'].apply(nominal_value)

L_H_COMB = FluxComponentAnalysis.L_H_COMB

df = df.assign(diff_col=df['precursor ' + L_H_COMB] - df['biomass ' + L_H_COMB])
df = df.rename(columns={'diff_col': '$g^{diss}$ (precursors -> biomass)'})

df1 = pd.DataFrame([df['biomass ' + L_H_COMB],
                    df['fermentation ' + L_H_COMB],
                    df['$g^{diss}$ (precursors -> biomass)'],
                    df[FluxComponentAnalysis.L_G_DISS],
                    -df['import ' + L_H_COMB]]).transpose()

width = max(ind) / 100.0
ax.set_prop_cycle(color=sns.color_palette('deep', n_colors=4))
ax.plot(ind, df1.iloc[:, -1].apply(nominal_value),
        label='glucose heat of combustion', marker='o', linewidth=0,
        markersize=10, color=(0.8, 0.6, 0.8), zorder=0)

ax.bar(ind, df1.iloc[:, 0].apply(nominal_value),
       width, yerr=df1.iloc[:, 0].apply(std_dev),
       bottom=0,
       label='stored in biomass')
ax.bar(ind, df1.iloc[:, 1].apply(nominal_value),
       width, yerr=df1.iloc[:, 1].apply(std_dev),
       bottom=df1.iloc[:, 0].apply(nominal_value),
       label='stored in fermentation')
ax.bar(ind, df1.iloc[:, 2].apply(nominal_value),
       width, yerr=df1.iloc[:, 2].apply(std_dev),
       bottom=df1.iloc[:, 0:2].applymap(nominal_value).sum(axis=1),
       label='dissipated (precursors -> biomass)')
ax.bar(ind, df1.iloc[:, 3].apply(nominal_value),
       width, yerr=df1.iloc[:, 3].apply(std_dev),
       bottom=df1.iloc[:, 0:3].applymap(nominal_value).sum(axis=1),
       label='dissipated (glucose -> precursors)')

ax.set_xlabel('growth rate [h$^{-1}$]')
ax.legend(loc='upper left')
ax.set_ylabel('kJ gCDW$^{-1}$ h$^{-1}$')
ax.set_xlim(0, 1)

# ax.plot([0, 1], [0, HEAT_OF_COMBUSTION_BIOMASS], '-')

# convert all dissipation and oxidation energies to units of
# percent of the total (glucose oxidation energy)

df1 = df1.div(df1.iloc[:, -1], axis=0)

ax = axs[1, 1]
ind = df['growth rate [h-1]'].apply(nominal_value)
width = max(ind) / 100.0
ax.set_prop_cycle(color=sns.color_palette('deep', n_colors=4))
ax.bar(ind, df1.iloc[:, 0].apply(nominal_value),
       width, yerr=df1.iloc[:, 0].apply(std_dev),
       bottom=0,
       label='stored in biomass')
ax.bar(ind, df1.iloc[:, 1].apply(nominal_value),
       width, yerr=df1.iloc[:, 1].apply(std_dev),
       bottom=df1.iloc[:, 0].apply(nominal_value),
       label='stored in fermentation')
ax.bar(ind, df1.iloc[:, 2].apply(nominal_value),
       width, yerr=df1.iloc[:, 2].apply(std_dev),
       bottom=df1.iloc[:, 0:2].applymap(nominal_value).sum(axis=1),
       label='dissipated (precursors -> biomass)')
ax.bar(ind, df1.iloc[:, 3].apply(nominal_value),
       width, yerr=df1.iloc[:, 3].apply(std_dev),
       bottom=df1.iloc[:, 0:3].applymap(nominal_value).sum(axis=1),
       label='dissipated (glucose -> precursors)')

ax.set_xlim(0, 1)
ax.plot([0, 1], [1, 1], '-', linewidth=4, color=(0.8, 0.6, 0.8), zorder=0)
ax.set_xlabel('growth rate [h$^{-1}$]')
ax.set_ylabel('fraction of glucose heat of combustion')
ax.legend(loc='upper left')

fig.tight_layout()
fig.savefig(os.path.join(RESULT_DIR, 'gdiss.pdf'))
