# MIT License
#
# Copyright (c) 2019 Elad Noor
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from uncertainties import nominal_value, std_dev

from .flux_component_analysis import FluxComponentAnalysis

sys.path.append(os.path.expanduser('~/git/flux-components/src'))

fca = FluxComponentAnalysis()

# %%
fca = FluxComponentAnalysis()
merged_df = fca.meta_df
df = merged_df[(merged_df['is_absolute_flux'] == True) &
               merged_df['Biomass Model'].isin(['e_coli', 'e_coli_ishii', 'e_coli_fiatflux']) &
               (merged_df['species'] == 'E. coli') &
               (merged_df['mutation'] == 'wild-type')]

# %%

x = 'growth rate [h-1]';
xlabel = 'growth rate [h-1]'
y = '$g^{diss}$ [kJ gCDW$^{-1}$ h$^{-1}$]'

fig, axs = plt.subplots(2, 2, figsize=(10, 10), sharex=True, sharey=True)

group_cols = ['medium', 'condition', 'strain', 'original_source']
for ax, group_col in zip(axs.flat, group_cols):
    ax.set_title(group_col)
    ax.set_prop_cycle(color=sns.color_palette('Paired', n_colors=11))
    for gr, gr_df in df.groupby(group_col):
        ax.errorbar(gr_df[x].apply(nominal_value),
                    np.abs(gr_df[y].apply(nominal_value)),
                    xerr=(gr_df[x].apply(std_dev)),
                    yerr=(gr_df[y].apply(std_dev)),
                    fmt='o', linewidth=0.5,
                    markersize=5, label='%s (%d)' % (gr, gr_df.shape[0]))

    ax.set_xlim(0, None)
    ax.set_ylim(0, None)
    ax.legend(fontsize=6, loc='upper left')

axs[1, 0].set_xlabel(xlabel)
axs[1, 1].set_xlabel(xlabel)
axs[0, 0].set_ylabel(y)
axs[1, 0].set_ylabel(y)
fig.tight_layout()

fig.savefig('../res/gdiss_simple1.pdf')

# %%

fig, ax = plt.subplots(1, 1, figsize=(8, 6))
ax.set_prop_cycle(color=sns.color_palette('deep', n_colors=4))

for label in ['import $h^{comb}$',
              'precursor $h^{comb}$',
              'fermentation $h^{comb}$',
              '$g^{diss}$',
              ]:
    y = label + ' [kJ gCDW$^{-1}$ h$^{-1}$]'
    ax.errorbar(df[x].apply(nominal_value),
                np.abs(df[y].apply(nominal_value)),
                xerr=(df[x].apply(std_dev)),
                yerr=(df[y].apply(std_dev)),
                fmt='.', linewidth=0.5, label=label, zorder=2,
                markersize=7)

xrange = np.linspace(0.03, 0.9, 100)
ax.plot(xrange, xrange * FluxComponentAnalysis.BIOMASS_HEAT_OF_COMBUSTION, 'm-',
        label='biomass $h^{comb}$')

ax.set_xlim(0, None)
ax.set_ylim(0, None)
# ax.set_yscale('log')
ax.set_xlabel(xlabel)
ax.set_ylabel('kJ gCDW$^{-1}$ h$^{-1}$')
_, xmax = ax.axes.get_xlim()

ax.legend(loc='best');

fig.savefig('../res/gdiss_simple2.pdf')

# %%

x = 'growth rate [h-1]';
xlabel = 'growth rate [h-1]'
y = '$g^{diss}$ [kJ gCDW$^{-1}$ h$^{-1}$]'

fig, axs = plt.subplots(1, 3, figsize=(10, 4), sharex=False, sharey=True)

group_cols = ['medium', 'condition', 'strain']
for ax, group_col in zip(axs.flat, group_cols):
    ax.set_title(group_col)
    ax.set_prop_cycle(color=sns.color_palette('husl', n_colors=len(df[group_col].unique())))
    for gr, gr_df in df.groupby(group_col):
        ax.errorbar(gr_df[x].apply(nominal_value),
                    np.abs(gr_df[y].apply(nominal_value)),
                    xerr=(gr_df[x].apply(std_dev)),
                    yerr=(gr_df[y].apply(std_dev)),
                    fmt='o', linewidth=0.5,
                    markersize=4, label='%s (%d)' % (gr, gr_df.shape[0]))

    ax.set_xlim(0, None)
    ax.set_ylim(0, None)
    ax.legend(fontsize=5, loc='upper left')

axs[0].set_xlabel(xlabel)
axs[1].set_xlabel(xlabel)
axs[2].set_xlabel(xlabel)
axs[0].set_ylabel(y)
fig.tight_layout()

fig.savefig('../res/gdiss_noor_ecoli_groups.pdf')

# %%
x = 'growth rate [h-1]';
xlabel = 'growth rate [h-1]'
y = '$g^{diss}$ [kJ gCDW$^{-1}$ h$^{-1}$]'

fig, ax = plt.subplots(1, 1, figsize=(4, 3))
ax.set_prop_cycle(color=sns.color_palette('Paired', n_colors=11))
for gr, gr_df in df.groupby('original_source'):
    ax.errorbar(gr_df[x].apply(nominal_value),
                np.abs(gr_df[y].apply(nominal_value)),
                xerr=(gr_df[x].apply(std_dev)),
                yerr=(gr_df[y].apply(std_dev)),
                fmt='o', linewidth=0.5,
                markersize=5, label='%s (%d)' % (gr, gr_df.shape[0]))

ax.set_xlim(0, 1.0)
ax.set_ylim(0, None)
ax.plot([0, 1.0], [4.9, 4.9], 'r-', linewidth=0.5)
ax.plot([0, 1.0], [4.7, 4.7], 'r--', linewidth=0.5)
ax.plot([0, 1.0], [5.1, 5.1], 'r--', linewidth=0.5)
ax.legend(fontsize=4, loc='upper left')

ax.set_xlabel('Growh rate (h$^{-1}$)')
ax.set_ylabel('Gibbs energy dissipation\nrate (kJ gCDW$^{-1}$ h$^{-1}$)')
fig.tight_layout()

fig.savefig('../res/gdiss_noor_ecoli_source.pdf')

# %%

df = merged_df[(merged_df['is_absolute_flux'] == True) &
               (merged_df['Biomass Model'] == 'e_coli') &
               (merged_df['species'] == 'E. coli') &
               (merged_df['mutation'] == 'wild-type') &
               (merged_df['medium'] == 'M9 + glucose') &
               (merged_df['strain'] == 'MG1655') &
               (merged_df['condition'] == 'chemostat')]

x = 'growth rate [h-1]';
xlabel = 'growth rate [h-1]'
y = '$g^{diss}$ [kJ gCDW$^{-1}$ h$^{-1}$]'

fig, ax = plt.subplots(1, 1, figsize=(4, 3))

ax.errorbar(df[x].apply(nominal_value),
            np.abs(df[y].apply(nominal_value)),
            xerr=(df[x].apply(std_dev)),
            yerr=(df[y].apply(std_dev)),
            fmt='.', linewidth=0.5,
            markersize=5, color='k')

ax.plot([0, 0.65], [4.9, 4.9], 'r-', linewidth=0.5)
ax.plot([0, 0.65], [4.7, 4.7], 'r--', linewidth=0.5)
ax.plot([0, 0.65], [5.1, 5.1], 'r--', linewidth=0.5)
ax.set_xlim(0, 0.65)
ax.set_ylim(0, 7)

ax.set_xlabel('Growh rate (h$^{-1}$)')
ax.set_ylabel('Gibbs energy dissipation\nrate (kJ gCDW$^{-1}$ h$^{-1}$)')
fig.tight_layout()

fig.savefig('../res/gdiss_noor_ecoli_chemostat.pdf')

# %%

df = merged_df[(merged_df['is_absolute_flux'] == True) &
               merged_df['Biomass Model'].isin(['e_coli', 'e_coli_ishii', 'e_coli_fiatflux']) &
               (merged_df['species'] == 'E. coli') &
               (merged_df['mutation'] == 'wild-type') &
               (merged_df['medium'] == 'M9 + glucose') &
               (merged_df['strain'] == 'MG1655')]

x = 'growth rate [h-1]';
xlabel = 'growth rate [h-1]'
y = '$g^{diss}$ [kJ gCDW$^{-1}$ h$^{-1}$]'

fig, ax = plt.subplots(1, 1, figsize=(4, 3))

ax.set_prop_cycle(color=sns.color_palette('Paired', n_colors=11))
for gr, gr_df in df.groupby('condition'):
    ax.errorbar(gr_df[x].apply(nominal_value),
                np.abs(gr_df[y].apply(nominal_value)),
                xerr=(gr_df[x].apply(std_dev)),
                yerr=(gr_df[y].apply(std_dev)),
                fmt='o', linewidth=0.5,
                markersize=5, label='%s (%d)' % (gr, gr_df.shape[0]))

ax.set_xlim(0, 1.0)
ax.set_ylim(0, None)
ax.plot([0, 1.0], [4.9, 4.9], 'r-', linewidth=0.5)
ax.plot([0, 1.0], [4.7, 4.7], 'r--', linewidth=0.5)
ax.plot([0, 1.0], [5.1, 5.1], 'r--', linewidth=0.5)
ax.legend(fontsize=6, loc='upper left')

ax.set_xlabel('Growh rate (h$^{-1}$)')
ax.set_ylabel('Gibbs energy dissipation\nrate (kJ gCDW$^{-1}$ h$^{-1}$)')
fig.tight_layout()

fig.savefig('../res/gdiss_noor_ecoli_MG1655_conditions.pdf')

# %%

df = merged_df[(merged_df['is_absolute_flux'] == True) &
               merged_df['Biomass Model'].isin(['e_coli', 'e_coli_ishii', 'e_coli_fiatflux']) &
               (merged_df['species'] == 'E. coli') &
               (merged_df['mutation'] == 'wild-type') &
               (merged_df['medium'] == 'M9 + glucose')]

x = 'growth rate [h-1]';
xlabel = 'growth rate [h-1]'
y = '$g^{diss}$ [kJ gCDW$^{-1}$ h$^{-1}$]'
c = 'fermentation $h^{comb}$ [kJ gCDW$^{-1}$ h$^{-1}$]'

fig, ax = plt.subplots(1, 1, figsize=(7, 4))
sns.scatterplot(x=df[x].apply(nominal_value),
                y=df[y].apply(nominal_value),
                hue=df[c].apply(nominal_value).fillna(0),
                palette='viridis',
                ax=ax)
ax.set_xlim(0, 1.0)
ax.set_ylim(0, None)
ax.plot([0, 1.0], [4.9, 4.9], 'r-', linewidth=0.5)
ax.plot([0, 1.0], [4.7, 4.7], 'r--', linewidth=0.5)
ax.plot([0, 1.0], [5.1, 5.1], 'r--', linewidth=0.5)
ax.legend(fontsize=4, loc='upper left')

ax.set_xlabel('Growh rate (h$^{-1}$)')
ax.set_ylabel('Gibbs energy dissipation\nrate (kJ gCDW$^{-1}$ h$^{-1}$)')
fig.tight_layout()

fig.savefig('../res/gdiss_noor_ecoli_acetate_overflow.pdf')
