# MIT License
#
# Copyright (c) 2019 Elad Noor
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import numpy as np
import pandas as pd
import sympy
from xlrd import XLRDError

from .settings import ureg, get_data


class BiomassComposition(object):
    METADATA_FNAME = '../data/meta_analysis.xls'

    def __init__(self, organism='e_coli_fixed'):
        self.organism = organism

        if self.organism is not None:
            sheet_name = self.organism + "_biomass"
            try:
                self.precursor_df = get_data(sheet_name=sheet_name,
                                             index_col=0, header=0).fillna(0)
            except XLRDError:
                raise ValueError(
                    f"There is no biomass function for '{self.organism}'")

            # a Series containing the growth dependence function for each sector
            self.growth_dependence = self.precursor_df['growth dependence'].apply(
                lambda s: sympy.sympify(s))

            # a DataFrame with the required amount of each precursor in mmol
            # per 1 gram of that sector
            self.precursor_df = self.precursor_df.iloc[:, 1:]
            self.precursor_df = self.precursor_df.applymap(
                lambda x: ureg.Measurement(x, 0, "mmol / gram")
            )

    @ureg.check(None, "1/[time]")
    def GetSectorCorrections(self, growth_rate: ureg.Measurement) -> pd.Series:
        """
            Returns a dataframe with the sector correction factors, which are
            the only growth-rate dependent part
        """
        mu = sympy.symbols('mu')

        sectors = {}
        for sect, func in self.growth_dependence.items():
            # sympy does not work with units and uncertainties, so we must
            # make tha calculations in floats and convert back to pint
            # Measurement afterwards.

            mu_nom = (growth_rate.value * ureg.hour).magnitude
            mu_std = (growth_rate.error * ureg.hour).magnitude

            if mu not in func.atoms():
                factor = func.evalf()
                uncert = 0.0
            elif np.isnan(mu_nom) or np.isnan(mu_std):
                raise Exception('Growth rate dependent biomass function '
                                '%s requires growth rate as input (not NaN)'
                                % self.organism)
            else:
                factor = func.evalf(subs={mu: mu_nom})
                uncert = np.abs(
                    sympy.diff(func).evalf(subs={mu: mu_nom})) * mu_std

            sectors[sect] = ureg.Measurement(factor, uncert)

        return pd.Series(sectors, name="relative_flux")

    @ureg.check(None, "1/[time]")
    def GetComposition(self, growth_rate: ureg.Measurement) -> pd.Series:
        """
            Returns a Series containing values of each precursor in the biomass
            in units of mmol per gram of cell dry weight
        """
        if self.organism is None:
            return pd.Series()
        else:
            # melt the precursor table and add the growth dependence factors
            # to each row depending on the biomass sector
            sector_correction_factors = self.GetSectorCorrections(growth_rate)
            biomass_df = self.precursor_df.reset_index().melt(
                id_vars="sector", var_name="precursor",
                value_name="mass_fraction")
            biomass_df = biomass_df.join(sector_correction_factors.to_frame(),
                                         on="sector")

            # multiply each sector by the corresponding correction factor
            biomass_df["flux"] = \
                biomass_df.mass_fraction * biomass_df.relative_flux

            # sum up the fluxes for each precursor
            biomass_df = biomass_df.groupby("precursor").sum()

            return biomass_df.flux

    @ureg.check(None, "1/[time]")
    def GetPrecursorBiomassFluxes(
            self,
            growth_rate: ureg.Measurement
    ) -> pd.Series:
        """
            Returns a Series containing rate of consumption of each precursor
            in units of mmol per gram of cell dry weight per hour
        """
        composition = self.GetComposition(growth_rate)
        return composition.apply(lambda x: x * growth_rate)


if __name__ == '__main__':
    ebc = BiomassComposition('e_coli_ishii')
    print(ebc.GetComposition(ureg.Measurement(np.nan, np.nan, "1/hour")))

    ebc = BiomassComposition('b_subtilis')
    print(ebc.GetComposition(ureg.Measurement(0.5, 0.05, "1/hour")))

    zbc = BiomassComposition('z_mobilis')
    print(zbc.GetComposition(ureg.Measurement(0.31, 0.0074, "1/hour")))
