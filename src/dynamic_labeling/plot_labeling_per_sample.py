# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 14:25:17 2015

@author: noore
"""
import sys
sys.path.append('/home/noore/git/ms-tools/lcms')
from isotope_util import compute_fractions
import openbis

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

sns.set()
sns.set_style("whitegrid")

CURVE_P0 = np.array([1.0, 1.0, 1.0, 1.0], dtype=float)
SIGPARAM_BOUNDS = ([0, 0, -100, 0], [1, 100, 100, 100])
SHOW_AVERAGES = True
EXPERIMENT_ID = 'E223070'


def sigmoid(x, a0, a1, a2, a3):
    return a0 * (1.0 + np.exp(-a1 * x)) / (a2 + np.exp(-a3 * x))

##############################################################################


samples_df = openbis.download_metadata(EXPERIMENT_ID)

# remove the control samples
samples_df = samples_df.loc[samples_df[openbis.STRAIN_COL_NAME] != "CONTROL", :]
samples_df.to_csv("res/sample_metadata.csv")

compounds, colors, styles = zip(*[
        ('HXP', "magenta", "-"),
        ('FBP', "blue", "-"),
        ('T3P', "cyan", "-"),
        ('xPG', "teal green", "-"),
        ('PEP', "orange", "-"),
        ('PYR', "red", "-"),
        ('LAC', "grey", "--"),
        ('SER', "grey", ":"),
])

colpalette = sns.xkcd_palette(colors)

# use "export to KNIME" file in mssoftware integration output menu
count_df = pd.read_csv(
    f'data/integration_results_{EXPERIMENT_ID}.txt',
    delimiter='\t', index_col=0
)
count_df.fillna(0, inplace=True)

# Calculate the fractional labels
fraction_df = count_df[list(compounds)].copy()
for c in compounds:
    fraction_df.loc[:, c] = count_df[c + "-13C"] / (count_df[c] + count_df[c + "-13C"])

# export the corrected 13C labelling data
fraction_df = fraction_df.join(samples_df)
fraction_df.to_csv('res/fractional_labelling.csv')

##################### PLOTTING #######################
print(f"Generating dynamic labeling plots:")

fig, axs = plt.subplots(1, 2, figsize=(15, 8))

for ax, (strain, strain_df) in zip(
        axs.flat, fraction_df.groupby(openbis.STRAIN_COL_NAME)):
        
    # in this experiment we don't have repeats, so we plot the raw data
    # directly without averaging and error-bars
    df = strain_df.sort_values(openbis.TIME_COL_NAME)
    for compound, color, style in zip(compounds, colpalette, styles):
        df.plot(x=openbis.TIME_COL_NAME, y=compound,
                color=color, linewidth=1, ax=ax, label=compound,
                marker="o", linestyle=style)
    
    ax.set_xlabel(openbis.TIME_COL_NAME)
    ax.set_ylabel('fractional labelling')
    ax.xaxis.grid(False)
    ax.set_ylim(0, 1.1)
    ax.set_xlim(0, 1.1)
    ax.set_title(strain)
    ax.legend()

fig.tight_layout()
fig.savefig('res/labeling.pdf')

