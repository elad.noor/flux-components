# MIT License
#
# Copyright (c) 2019 Elad Noor
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import seaborn as sns
from typing import Union, List
import numpy as np
import pandas as pd
from scipy.linalg import eig, inv, expm
import sympy
import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams['text.usetex'] = True
from matplotlib.backends.backend_pdf import PdfPages

import sys
sys.path.append('/home/noore/git/ms-tools/lcms')
import openbis

OUTPUT_PDF_FNAME = "res/simulated_data.pdf"

class DynamicLabeling(object):

    def __init__(
            self,
            fluxes: np.ndarray,
            concentrations: np.ndarray,
            legend: List[str],
            initial_condition: np.ndarray = None,
    ):
        self.n_pools = fluxes.shape[0]
        assert self.n_pools == fluxes.shape[1]
        assert concentrations.shape == (self.n_pools, )

        self.fluxes = fluxes
        self.concentrations = concentrations
        self.legend = legend

        if initial_condition is None:
            self.initial_condition = np.array([0] + [1] * (self.n_pools - 1))
        else:
            assert initial_condition.shape == (self.n_pools, )
            self.initial_condition = initial_condition

        self.M = np.diag(1.0 / self.concentrations) @ (
                self.fluxes.T - np.diag(self.fluxes.sum(0))
        )

    def find_endpoint(self, threshold: float = 1e-4) -> float:
        """Find the time where all labeling values are below the threshold."""
        w, _ = eig(self.M[1:, 1:])
        return np.log(threshold) / max(w.real)

    def simulate(self, t: Union[float, np.ndarray]) -> np.ndarray:
        if type(t) == float:
            return expm(t * self.M) @ self.initial_condition
        else:
            exponents = [expm(_t * self.M) for _t in t.flat]
            return np.array(exponents) @ self.initial_condition

    def plot_simulation(self) -> None:
        """make a sympy representation of 'f'

        :param max_t: maximum time point to simulate
        """
        max_t = self.find_endpoint()
        tspace = np.linspace(0, max_t, 100)
        specdata = self.simulate(tspace)
        fig, axs = plt.subplots(2, 1, figsize=(6, 6),
                                gridspec_kw={'height_ratios': [3, 1]})
        ax = axs.flat[0]
        sns.set_palette(sns.color_palette("muted",
                                          n_colors=specdata.shape[1]))
        ax.plot(tspace, specdata)
        if self.legend:
            ax.legend(self.legend)

        ax = axs.flat[1]
        s_M = sympy.latex(sympy.Matrix(self.M.round(2)), mode="plain",
                          mat_str="array")
        s_f0 = sympy.latex(sympy.Matrix(self.initial_condition.round(2)),
                           mode="plain", mat_str="array")
        ax.text(0.5, 0.5, f"$f(t) = \\exp \\left( t {s_M} \\right) {s_f0}$",
                ha="center", va="center", fontsize=6)
        ax.set_xlabel("time (s)")
        ax.set_ylabel("unlabeled fraction")
        ax.axis("off")

        return fig

    def plot_versus_f1(self, mets_to_show: List[str] = None):
        max_t = self.find_endpoint()
        tspace = np.linspace(0, max_t, 100)
        specdata = self.simulate(tspace)
        fig, ax = plt.subplots(1, 1, figsize=(6, 6))
        sns.set_palette(sns.color_palette("muted",
                                          n_colors=specdata.shape[1]))

        if mets_to_show is None:
            mets_to_show = self.legend[2:]

        idx = [self.legend.index(m) for m in mets_to_show]
        ax.plot(specdata[:, 1], specdata[:, idx])
        ax.legend(mets_to_show)
        ax.plot([0, 1], [0, 1], "k:")
        ax.set_xlabel(f"Unlabaled fraction of {self.legend[1]}")
        ax.set_ylabel(f"Unlabaled fraction of downstream metabolite")
        return fig

with PdfPages(OUTPUT_PDF_FNAME) as pdf:

    #%%

    # TODO: read the fluxes from the meta_analysis.xls file
    v_for = np.array([9.6540, # PTS
                      5.700,  # PGI
                      7.0585, # PFK
                      7.0585, # FBA
                      7.0585, # TPI
                      15.7104, # GAPDH
                      15.7104, # PGK
                      14.5577, # PGM
                      14.5577, # ENO
                      2.4873+9.6540, # PYK + PTS
                      11.2985, # PDH
                     ], dtype=float)  # in units of mmol / gCDW / h

    v_for *= (1000.0 / 60.0) # convert to μmol / gCDW / min

    # assume no backward fluxes
    v_rev = np.zeros(v_for.shape[0])
    V = np.diag(v_for, k=1) + np.diag(v_rev, k=-1)

    cell_density = 0.3  # gCDW / mL (for converting fluxes from μmol / gCDW / min
                        # to mM/min)

    V_mM_per_min = V * cell_density

    #%%
    # Gerosa 2015, glucose condition

    metabolomics_df = pd.read_excel(
        "data/gerosa2015_steady_state.xlsx",
        sheet_name="Metabolite concentrations",
        header=1
    )
    metabolomics_df = metabolomics_df.set_index("Metabolite name (short)")
    metabolomics_df.rename(index={"2pg+3pg": "3pg"}, inplace=True)

    condition = "Glucose"
    mets = ["glucose", "g6p", "f6p", "fdp", "dhap", "gap", "13dpg", "2pg",
            "3pg", "pep", "pyr", "accoa"]

    s = metabolomics_df.reindex(mets)["Glucose"]  # in units of μmol / gCDW

    m = np.nanmean(s)
    s["glucose"] = 5000.0  # μmol / gCDW
    s["gap"] = m
    s["accoa"] = m
    s["2pg"] = s["3pg"]/2.0
    s["3pg"] = s["2pg"]

    dyn = DynamicLabeling(V, s.values, legend=mets)

    #fig = dyn.plot_simulation()
    fig = dyn.plot_versus_f1(["fdp", "gap", "3pg", "pep", "pyr"])
    fig.suptitle("Metabolomis and fluxes from Gerosa 2015")
    pdf.savefig(fig)
    plt.close(fig)

    #%%
    # Kochanowski 2019, glucose condition

    metabolomics_df = pd.read_excel(
        "data/kochanowski_metabolites.xlsx",
        sheet_name="absolute data",
        header=1, index_col=0)
    i_break = np.where(pd.isnull(metabolomics_df.index))[0][0]
    metabolomics_df = metabolomics_df.iloc[0:i_break, :]

    metabolomics_df.rename(index={"xPG": "3PG"}, inplace=True)

    mets = ["GLUCOSE", "G6P", "F6P", "FBP", "DHAP", "GAP", "BPG", "2PG",
            "3PG", "PEP", "PYR", "Acetyl-CoA"]

    s = metabolomics_df.reindex(mets)[" M9 glucose 2g per L first experiment"] # mM

    # fill all nan values with the mean of all other values
    m = np.nanmean(s)
    s[pd.isnull(s)] = m

    # override a few of the values based on logical assumptions
    s["GLUCOSE"] = 1000.0  # mM
    s["2PG"] = s["3PG"]/2.0
    s["3PG"] = s["3PG"]/2.0
    s["Acetyl-CoA"] = m

    dyn = DynamicLabeling(V_mM_per_min, s.values, legend=mets)

    #fig = dyn.plot_simulation()
    fig = dyn.plot_versus_f1(["FBP", "GAP", "3PG", "PEP", "PYR"])
    fig.suptitle("Fluxes from Gerosa 2015, metabolomics from Kochanowski 2019")
    pdf.savefig(fig)
    plt.close(fig)

    #%%
    # Bennett 2009, glucose condition

    metabolomics_df = pd.read_csv(
        "data/bennett_2009_metabolites.csv",
        index_col=0
    )

    mets = ["glucose", "g6p", "f6p", "fbp", "dhap", "gap", "13dpg", "2pg",
            "3pg", "pep", "pyr", "accoa"]

    s = metabolomics_df.reindex(mets)["concentration_mM"] # mM

    # fill all nan values with the mean of all other values
    m = np.nanmean(s)
    s[pd.isnull(s)] = m

    # override a few of the values based on logical assumptions
    s["2pg"] = s["3pg"]/2.0
    s["3pg"] = s["3pg"]/2.0

    s = s + np.random.random(s.shape)*1e-4

    dyn = DynamicLabeling(V_mM_per_min, s.values, legend=mets)

    #fig = dyn.plot_simulation()
    fig = dyn.plot_versus_f1(["fbp", "gap", "3pg", "pep", "pyr"])
    fig.suptitle("Fluxes from Gerosa 2015, metabolomics from Bennett 2009")
    pdf.savefig(fig)
    plt.close(fig)


    #%% data from last experiment
    # use "export to KNIME" file in mssoftware integration output menu
    EXPERIMENT_ID = 'E223070'
    compounds = ["HXP", "FBP", "T3P", "xPG", "PEP", "PYR"]

    samples_df = openbis.download_metadata(EXPERIMENT_ID)
    samples_df = samples_df.loc[
                 samples_df[openbis.STRAIN_COL_NAME] != "CONTROL", :]

    count_df = pd.read_csv(
        f'data/integration_results_{EXPERIMENT_ID}.txt',
        delimiter='\t', index_col=0
    )
    count_df.fillna(0, inplace=True)

    # Calculate the **unlabeled** fractions
    fraction_df = count_df[list(compounds)].copy()
    for c in compounds:
        fraction_df.loc[:, c] = \
            count_df[c] / (count_df[c] + count_df[c + "-13C"])

    # export the corrected 13C labelling data
    fraction_df = fraction_df.join(samples_df)

    fig, axs = plt.subplots(1, 2, figsize=(10, 5))

    sns.set_palette(sns.color_palette("muted", n_colors=len(compounds)))

    for ax, (strain, strain_df) in zip(
            axs.flat, fraction_df.groupby(openbis.STRAIN_COL_NAME)):
        f1_compound = compounds[0]

        # in this experiment we don't have repeats, so we plot the raw data
        # directly without averaging and error-bars
        df = strain_df.sort_values(f1_compound).copy()
        for compound in compounds[1:]:
            df.plot(x=f1_compound, y=compound, ax=ax, label=compound,
                    marker="o")

        ax.set_xlabel(f"unlabaled fraction of {f1_compound}")
        ax.set_ylabel("unlabaled fraction")
        ax.xaxis.grid(False)
        ax.set_ylim(0, 1)
        ax.set_xlim(0, 1)
        ax.set_title(strain.replace("_", "-"))
        ax.legend()

    fig.tight_layout()
    pdf.savefig(fig)
    plt.close(fig)
