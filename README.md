# flux-components
Analyzing the naturally occuring flux space of microbes

## Requirements
- sympy
- xlrd
- uncertainties
- numpy
- scipy
- sklearn
- seaborn
- pint
- cobamp
- jupyterlab
- cvxopt
- escher>=1.7.1
- cobra>=0.15.4
- equilibrator-api>=0.2.5

## For using Escher in Jupyter Lab or Notebooks:
* Use the `setup.sh` script or ...
* Follow the [instructions on the Escher site](https://cnpmjs.org/package/escher).


