#!/usr/bin/env bash
pip install -r requirements.txt
pushd /opt/ibm/ILOG/CPLEX_Studio129/cplex/python/3.7/x86-64_linux
python setup.py install
popd
jupyter nbextension install --py escher
jupyter nbextension enable --py escher
jupyter labextension install @jupyter-widgets/jupyterlab-manager
jupyter labextension install escher
